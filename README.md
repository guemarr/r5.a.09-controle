
  

# TP Noté Virtualisation Avancée

## Installation 

Pour pouvoir connecter les logs veuillez installer cette extension pour Loki : 

    docker plugin install grafana/loki-docker driver:latest --alias loki --grant-all-permissions

  

## Services


| Nom du Services | PORT |
|--|--|
| Treafik | 8080 |
| Loki ( + Grafana et promtail ) | 3000 |
| Joomla ( + JoomlaDb ) | 8081 |
| Gitea | 3001 |
| Matomo | 8082 |
| Odoo(+bd) | 8069 |


## Logs
Le label logging à été ajouter dans chaque services pour pouvoir permettre à Loki de collecter tout les logs disponible sur l'interface Grafana


## Traefik 

Je n'ai pas réussi à relier les services à Traefik une fois les config faites les conteneurs refusait de se fermer j'ai néanmoins mis les fichiers dans un dossier v2 



