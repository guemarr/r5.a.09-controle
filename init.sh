#!/bin/bash

docker compose -f gitea/docker-compose.yml up -d
docker compose -f joomla/docker-compose.yml up -d
docker compose -f loki/docker-compose.yml up -d
docker compose -f matomo/docker-compose.yml up -d
docker compose -f odoo/docker-compose.yml up -d
docker compose -f traefik/docker-compose.yml up -d